% The directory where you extracted the raw dataset.
datasetDir = ['..' filesep '..' filesep 'data'];

% The name of the scene to demo.
%sceneName = 'dining_room_0036';
sceneName = 'kinectv1_0006';

% The absolute directory of the 
sceneDir = [datasetDir filesep sceneName];

% The output directory
outputDir = ['..' filesep '..' filesep 'data' filesep 'kinectv1_0006_synched'];
% Make sure that the output directories exist
exists_or_mkdir(outputDir);
exists_or_mkdir([outputDir filesep 'images']);
exists_or_mkdir([outputDir filesep 'rawdepth']);
exists_or_mkdir([outputDir filesep 'depth']);
exists_or_mkdir([outputDir filesep 'depth_std2p']);
exists_or_mkdir([outputDir filesep 'depth_noise']);
exists_or_mkdir([outputDir filesep 'images_hq']);
exists_or_mkdir([outputDir filesep 'rawdepth_hq']);
exists_or_mkdir([outputDir filesep 'depth_hq']);
exists_or_mkdir([outputDir filesep 'depth_std2p_hq']);
exists_or_mkdir([outputDir filesep 'depth_noise_hq']);

% Reads the list of frames.
frameList = get_synched_frames(sceneDir);

% Frame step (how many to skip)
%frameStep = 1500; % For quick testing
frameStep = 1; % STD2P has 3
maxFrames = numel(frameList); % STD2P has 101
skipFrames = 0; % To start generate from different position STD2P has 0

% True when you don't want to save any files
testing = false;

sMessage = sprintf('Scene has %d total frames', numel(frameList));
disp(sMessage)

sMessage = sprintf('Started generating %d frames for scene %s starting from frame %d and having step %d', maxFrames, sceneName, (skipFrames + 1), frameStep);
disp(sMessage)
frameCounter = 0;

scaleFromMMtoM = 1000;

% STD2P uses this factor to calculate the depth data to better take
% advantage of the uint16 data type
camera_params;
std2pFactor = double(intmax('uint16')) / (maxDepth * scaleFromMMtoM);

% Saves each pair of synchronized RGB and Depth frames.
for ii = (1 + skipFrames) : frameStep : numel(frameList)

  if (frameCounter == maxFrames) break; end
  imgRgb = imread([sceneDir '/' frameList(ii).rawRgbFilename]);
  imgDepthRaw = swapbytes(imread([sceneDir '/' frameList(ii).rawDepthFilename]));
  % Generate the projected depth image.
  %[imgDepthProj, imgRgbProj] = project_depth_map(imgDepthRaw, imgRgb);
  [imgDepthProj, imgRgbProj, imgDepthNoise] = project_depth_map_v2(imgDepthRaw, imgRgb);
  % Inpaint the projected depth image. (using any of Levin et al, or cross-bf methods)
  alphaLevinEtAl = 0.1; % Like the default in fill_depth_colorization
  %size(imgRgb)
  %size(imgDepthProj)
  imgRgbNorm = imNormalize(imgRgb, 2);
  imgRgbProjNorm = imNormalize(imgRgbProj, 2);
  % imgDepthInpainted = fill_depth_colorization(imgRgbNorm, imgDepthProj, alphaLevinEtAl);
  imgDepthInpainted = fill_depth_colorization(imgRgbProjNorm, imgDepthProj, alphaLevinEtAl);
  % imgDepthInpainted = fill_depth_cross_bf(imgRgb, double(imgDepthProj));  
  
  % Save the image, rawDepth (the folder structure follows the structure used in rcnn-depth.
  filename = sprintf('%04d', ii);
  sMessage = sprintf('Generating frame %s which is %d/%d for this sequence', filename, (frameCounter + 1), maxFrames);
  disp(sMessage)
  if (testing == false) 
    imwrite(uint8(cropIt(imgRgb)), [outputDir '/images/img_' filename '.png']);
    imwrite(uint8(imgRgb), [outputDir '/images_hq/img_' filename '.png']);
    
    %imgDepthRaw = imNormalize(double(imgDepthRaw), 2);
    %imgDepthRaw(imgDepthRaw==1) = 0; % Make white spots, black like rcnn-depth
    
    % It turns out that NYUD2 raw depth is the projected depth with the Kinect depth non-linearity removed
    %imwrite(uint16(cropIt(imgDepthRaw)), [outputDir filesep 'rawdepth' filesep 'img_' filename '.png']);
    %imwrite(uint16(imgDepthRaw), [outputDir filesep 'rawdepth_hq' filesep 'img_' filename '.png']);
    imwrite(uint16(cropIt(imgDepthProj) * scaleFromMMtoM), [outputDir filesep 'rawdepth' filesep 'img_' filename '.png']);
    imwrite(uint16(imgDepthProj * scaleFromMMtoM), [outputDir filesep 'rawdepth_hq' filesep 'img_' filename '.png']);
    
    % Save the projected and inpainted depth image.
    % imgDepthInpainted = imNormalize(imgDepthInpainted, 2);
    imwrite(uint16(cropIt(imgDepthInpainted) * scaleFromMMtoM), [outputDir '/depth/img_' filename '.png']);
    imwrite(uint16(imgDepthInpainted*scaleFromMMtoM), [outputDir '/depth_hq/img_' filename '.png']);
    
    % Save the std2p depth image
    imwrite(uint16(cropIt(imgDepthInpainted) * scaleFromMMtoM * std2pFactor), [outputDir '/depth_std2p/img_' filename '.png']);
    imwrite(uint16(imgDepthInpainted*scaleFromMMtoM*std2pFactor), [outputDir '/depth_std2p_hq/img_' filename '.png']);
    
    % Save the projected depth noise
    imwrite(uint8(cropIt(imgDepthNoise)), [outputDir filesep 'depth_noise' filesep 'img_' filename '.png']);
    imwrite(uint8(imgDepthNoise), [outputDir filesep 'depth_noise_hq' filesep 'img_' filename '.png']);
  end

  frameCounter = frameCounter + 1;

end

disp('Finished generating')
