addpath(genpath(['..' filesep 'scripts']));
read_config_params;

% The directory where you extracted the raw dataset.
datasetDir = ['..' filesep '..' filesep 'data'];

% The absolute directory of the 
sceneDir = [datasetDir filesep sceneName];

% The output directory
outputDir = ['..' filesep '..' filesep 'data' filesep sceneName '_synched'];

% Make sure that the output directories exist
exists_or_mkdir(outputDir);
exists_or_mkdir([outputDir filesep 'images']);
exists_or_mkdir([outputDir filesep 'rawdepth']);
exists_or_mkdir([outputDir filesep 'depth']);
exists_or_mkdir([outputDir filesep 'depth_std2p']);
exists_or_mkdir([outputDir filesep 'depth_noise']);
exists_or_mkdir([outputDir filesep 'images_hq']);
exists_or_mkdir([outputDir filesep 'rawdepth_hq']);
exists_or_mkdir([outputDir filesep 'depth_hq']);
exists_or_mkdir([outputDir filesep 'depth_std2p_hq']);
exists_or_mkdir([outputDir filesep 'depth_noise_hq']);

frameListFile = ['.' filesep 'frame_list.txt'];

% If you want to check something quickly
%batchId = 1;
%batchSize = 1;

if (exist('generateFrameList') == 1)
  % Reads the list of frames.
  frameList = get_synched_frames(sceneDir);
  % Save the frame list (for python to know who to spawn)
  fileID = fopen(frameListFile, 'w');
  for ii = 1 : numel(frameList)
    frame = sprintf('%04d', ii);
    fprintf(fileID,'%s\n', frame);
  end
  fclose(fileID);
  return;
else
  if (exist('batchId') == 1 & exist('batchSize') == 1)
    % Read the frame list 
    frameList = get_synched_frames(sceneDir);
    % Set the frame number and batch size
  else
    disp('You need to set frameNumber and batchSize to run this script.');
    return;
  end    
end

% Frame step (how many to skip)
%frameStep = 1500; % For quick testing
frameStep = 1; % To generate all the frames
skipFrames = (batchId - 1) * batchSize; % To start generate from different position STD2P has 0
% Generate up to the batch size or frames length whichever comes first
maxFrames = min(skipFrames + batchSize, numel(frameList));
%maxFrames = min(skipFrames + 1, numel(frameList));
% True when you don't want to save any files
testing = false;

sMessage = sprintf('Scene has %d total frames', numel(frameList));
disp(sMessage)

sMessage = sprintf('Started generating %d frames for scene %s starting from frame %d and having step %d', maxFrames, sceneName, (skipFrames + 1), frameStep);
disp(sMessage)
frameCounter = 0;

disp('Skip frames:');
disp(skipFrames);
%exit;

scaleFromMMtoM = 1000;

% STD2P uses this factor to calculate the depth data to better take
% advantage of the uint16 data type
camera_params;
std2pFactor = double(intmax('uint16')) / (maxDepth * scaleFromMMtoM);

% Saves each pair of synchronized RGB and Depth frames.
for ii = skipFrames + 1 : frameStep : maxFrames

  filename = sprintf('%04d', ii);
  if(exist([outputDir filesep 'images' filesep 'img_' filename '.png']) == 2)
    % Skip existing files
    disp(sprintf('Skipping %s', filename));
    continue;
  end

  imgRgb = imread([sceneDir filesep frameList(ii).rawRgbFilename]);
  imgDepthRaw = swapbytes(imread([sceneDir filesep frameList(ii).rawDepthFilename]));
  % Generate the projected depth image.
  %[imgDepthProj, imgRgbProj] = project_depth_map(imgDepthRaw, imgRgb);
  [imgDepthProj, imgRgbProj, imgDepthNoise] = project_depth_map_v2(imgDepthRaw, imgRgb);
  % Inpaint the projected depth image. (using any of Levin et al, or cross-bf methods)
  %alphaLevinEtAl = 1; % Like the default in fill_depth_colorization
  alphaLevinEtAl = 0.1; % Like STD2P
  %size(imgRgb)
  %size(imgDepthProj)
  imgRgbNorm = imNormalize(imgRgb, 2);
  imgRgbProjNorm = imNormalize(imgRgbProj, 2);
  %imgDepthInpainted = fill_depth_colorization(imgRgbNorm, imgDepthProj, alphaLevinEtAl);
  imgDepthInpainted = fill_depth_colorization(imgRgbProjNorm, imgDepthProj, alphaLevinEtAl); % Like STD2P
  % imgDepthInpainted = fill_depth_cross_bf(imgRgb, double(imgDepthProj));  

  % Save the image, rawDepth (the folder structure follows the structure used in rcnn-depth.
  sMessage = sprintf('Generating frame %s which is %d/%d for this sequence', filename, (frameCounter + 1), maxFrames);
  disp(sMessage)
  if (testing == false)
    imwrite(uint8(cropIt(imgRgb)), [outputDir filesep 'images' filesep 'img_' filename '.png']);
    imwrite(uint8(imgRgb), [outputDir filesep 'images_hq' filesep 'img_' filename '.png']);
    %imgDepthRaw = imNormalize(double(imgDepthRaw), 2);
    %imgDepthRaw(imgDepthRaw==1) = 0; % Make white spots, black like rcnn-depth

    % It turns out that NYUD2 raw depth is the projected depth with the Kinect depth non-linearity removed
    %imwrite(uint16(cropIt(imgDepthRaw)), [outputDir filesep 'rawdepth' filesep 'img_' filename '.png']);
    %imwrite(uint16(imgDepthRaw), [outputDir filesep 'rawdepth_hq' filesep 'img_' filename '.png']);
    imwrite(uint16(cropIt(imgDepthProj) * scaleFromMMtoM), [outputDir filesep 'rawdepth' filesep 'img_' filename '.png']);
    imwrite(uint16(imgDepthProj * scaleFromMMtoM), [outputDir filesep 'rawdepth_hq' filesep 'img_' filename '.png']);

    % Unnormalized and casted way (works with rcnn-depth)
    imwrite(uint16(cropIt(imgDepthInpainted) * scaleFromMMtoM), [outputDir filesep 'depth' filesep 'img_' filename '.png']);
    imwrite(uint16(imgDepthInpainted * scaleFromMMtoM), [outputDir filesep 'depth_hq' filesep 'img_' filename '.png']);
 
    % Normalized and uncasted way
    %imgDepthInpainted = imNormalize(imgDepthInpainted, 2);
    %imwrite(cropIt(imgDepthInpainted), [outputDir filesep 'depth' filesep 'img_' filename '.png']);
    %imwrite(imgDepthInpainted, [outputDir filesep 'depth_hq' filesep 'img_' filename '.png']);
    
    % Save the std2p depth image
    imwrite(uint16(cropIt(imgDepthInpainted) *scaleFromMMtoM * std2pFactor), [outputDir filesep 'depth_std2p' filesep 'img_' filename '.png']);
    imwrite(uint16(imgDepthInpainted * scaleFromMMtoM * std2pFactor), [outputDir filesep 'depth_std2p_hq' filesep 'img_' filename '.png']);

    % Save the projected depth noise
    imwrite(uint8(cropIt(imgDepthNoise)), [outputDir filesep 'depth_noise' filesep 'img_' filename '.png']);
    imwrite(uint8(imgDepthNoise), [outputDir filesep 'depth_noise_hq' filesep 'img_' filename '.png']);

  end

  frameCounter = frameCounter + 1;

end

disp('Finished generating')
